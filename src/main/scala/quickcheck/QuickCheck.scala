package quickcheck

import org.scalacheck.*
import Arbitrary.*
import Gen.*
import Prop.forAll

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap:


  property("min1") = forAll { (a: Int) =>
    val h = insert(a, empty)
    findMin(h) == a
  }

 /* property("gen1") = forAll { (h: H) =>
    val m = if isEmpty(h) then 0 else findMin(h)
    findMin(insert(m, h)) == m
  }*/


  property("insert to elements into empty heap") = forAll { (a: Int, b: Int) =>
    val first = insert(a, empty)
    val second = insert(b, first)
    findMin(second) == Math.min(a, b)
  }

  property("insert one element to heap and delete") = forAll { (a: Int) =>
    val b = insert(a, empty)
    deleteMin(b) == empty
  }

  property("minimum of the melding of tw0 different heaps") = forAll { (h1: H, h2: H) =>
    if h1 == empty || h2 == isEmpty then true
    else {
      val minH1 = findMin(h1)
      val minh2 = findMin(h2)
      val meldedHeaps = meld(h1, h2)
      findMin(meldedHeaps) == Math.min(minH1, minh2)
    }
  }
  property("equality melding of equal heap") = forAll { (h1: H, h2: H) =>
    def meldHeapChecker(h1: H, h2: H): Boolean =
      if isEmpty(h1) && isEmpty(h2) then true
      else {
        val minh1 = findMin(h1)
        val minh2 = findMin(h2)
        minh1 == minh2 && meldHeapChecker(deleteMin(h1), deleteMin(h2))
        }
    val meld1 = meld(h1, h2)
    val minH1 = findMin(h1)
    val meld2 = meld(deleteMin(h1), insert(minH1, h2))

      meldHeapChecker(meld1, meld2)
  }


    property("Given any heap, you should get a sorted sequence of elements when " +
      "continually finding and deleting minima") = forAll { (x: H) =>
      def checkHelper(heap: QuickCheckHeap.this.H): Boolean = {
        if heap == empty then true
        else {
          val min = findMin(heap)
          val newHeap = deleteMin(heap)
          newHeap == empty || (min <= findMin(newHeap) && checkHelper(newHeap))
        }
      }

      checkHelper(x)
    }


  lazy val genHeap: Gen[H] = for {
    value <- arbitrary[A]
    heap <- oneOf(const(empty), genHeap)
  } yield insert(value, heap)

  given Arbitrary[H] = Arbitrary(genHeap)


